# French translation of StatCan WxT

msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-04-27 03:47+0000\n"
"PO-Revision-Date: 2020-04-27 03:47+0000\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

msgid "Date modified:"
msgstr "Date de modification:"

msgid "Signed in as"
msgstr "Connecté en tant que"

msgid "Search Canada.ca"
msgstr "Rechercher dans Canada.ca"

# Trick the langauge switcher into using the French label, when the UI is in English.
msgid "French"
msgstr "Français"
